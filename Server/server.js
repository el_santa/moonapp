const express = require('express');
const app = express();
const api = require('./api');

app.listen(8000, () => {
	console.log('Server started!')
});


app.route('/api/calcBusinessDays').get((req, res) => {
	try {
		res.send(api.GetBusinessDays(req.params['Start_date'] || req.query['Start_date'], req.params['End_date'] || req.query['End_date']));
	} catch (error) {
		res.status(500).send(error.message);
	}
});


app.route('/api/getRate').get((req, res) => {
	try {
		res.send(api.GetRate(
			req.params['width'] || req.query['width'],
			req.params['height'] || req.query['height'],
			req.params['length'] || req.query['length'],
			req.params['weight'] || req.query['weight']
		));
	} catch (error) {
		res.status(500).send(error.message);
	}
});