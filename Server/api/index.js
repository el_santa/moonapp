const Utils = require('../Utils');

const ErrorMessage = {
	EmptyRequestPrameter: 'empty request parameter value',
	InvalidRequestParameter: 'invalid request parameter value'
};

//returns FULL business days count between two dates 
function getBusinessDays(startDateString, endDateString) {
	var businessDays = 0;

	var calcSettings = {
		weekend: [5, 6],
		workDayStartHour: 9,
		workDayEndHour: 18
	};

	var startDateStringFormated = Utils.FormatDateString(startDateString);
	var endDateStringFormated = Utils.FormatDateString(endDateString);

	//weekend days should be ordered by asc
	calcSettings.weekend = calcSettings.weekend.sort(function (a, b) { return a - b; });

	if (!startDateString || !endDateString) throw new Error(ErrorMessage.EmptyRequestPrameter);

	var startDate = new Date(startDateStringFormated);
	var endDate = new Date(endDateStringFormated);

	if(!Utils.IsValidDate(startDate) || !Utils.IsValidDate(endDate)) throw new Error(ErrorMessage.InvalidRequestParameter);

	var fullWeekStartDate = new Date(startDate);

	//set start date to weekend start and count businnes days if start date before weekend
	while (fullWeekStartDate.getDay() < calcSettings.weekend[0]) {
		if (fullWeekStartDate.getHours() < calcSettings.workDayStartHour)
			businessDays++;

		fullWeekStartDate.setDate(fullWeekStartDate.getDate() + 1);
		fullWeekStartDate.setHours(0);
		fullWeekStartDate.setMinutes(00);
	}

	//set start date to weekend start and count businnes days if start date into weekend
	while (fullWeekStartDate.getDay() > calcSettings.weekend[0]) {
		if (!calcSettings.weekend.includes(fullWeekStartDate.getDay()) && fullWeekStartDate.getHours() < calcSettings.workDayStartHour)
			businessDays--;

		fullWeekStartDate.setDate(fullWeekStartDate.getDate() - 1);
		fullWeekStartDate.setHours(23);
		fullWeekStartDate.setMinutes(59);
	}

	var fullWeekEndDate = new Date(endDate);

	//set end date to weekend start and count business days
	while (fullWeekEndDate.getDay() < calcSettings.weekend[0]) {
		if (!calcSettings.weekend.includes(fullWeekEndDate.getDate()) && fullWeekEndDate.getHours() > calcSettings.workDayEndHour)
			businessDays++;

		fullWeekEndDate.setDate(fullWeekEndDate.getDate() - 1);
		fullWeekEndDate.setHours(23);
		fullWeekEndDate.setMinutes(59);
	}

	//fuul weeks dif
	var diffDays = (fullWeekEndDate - fullWeekStartDate + 60000) / 1000 / 60 / 60 / 24;
	var weeks = Math.floor(diffDays / 7);

	businessDays += weeks * (7 - calcSettings.weekend.length);

	return {
		business_days: businessDays,
		Start_date: startDateString,
		End_date: endDateString
	};
}

//returns calculated shipping rate for package size and weight
function getRate(width, height, length, weight)
{
	return 'hello';
}


module.exports = {
	GetBusinessDays: getBusinessDays,
	GetRate: getRate
}