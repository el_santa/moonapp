function formatDateString(dateString) {
	if (dateString.length === 10)
		dateString += ' 00:00';

	return dateString;
}

function isValidDate(d) {
	return d instanceof Date && !isNaN(d);
}


module.exports = {
	FormatDateString: formatDateString,
	IsValidDate: isValidDate
}