import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuisnessDaysComponent } from './buisness-days/buisness-days.component';
import { FormsModule } from '@angular/forms'
import { PackageRateComponent } from './package-rate/package-rate.component';

@NgModule({
   declarations: [
      AppComponent,
      BuisnessDaysComponent,
      PackageRateComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
